var sww = {
    originalHost: null,
    host: null,
    slipObjectsArray: [], // is array of objects [{slip:'12345_123456789', img:'ikj...hf=='},{slip:'234_567',img:'idh...df=='}];
    user: null,
    DataCount: 0

};
var ptrn = {
    slipPtrn: null
};
var store = window.localStorage;
//=============================  Save Adult  =============================
function saveAdultData() {
    var data = tp.getData();
    if (!(data && isArray(data) && data.length > 0)) {
        showErrorMessage(eMsg.enterSignature);
        return false;
    }
//==================full_name==================\\
    var $full_name = $('#full_name');
    if (hasFieldError($full_name)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($full_name);
    }
    if ($full_name.val() === '') {

    }


//==================date==================\\
    var $date = $('#date');
    if (hasFieldError($date)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($date);
    }
    if ($date.val() === '') {

    }

    //==================printed name==================\\
    var $printed_name = $('#printed_name');
    if (hasFieldError($printed_name)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($printed_name);
    }
    if ($printed_name.val() === '') {

    }
    var $canvas = $('#touchpaint');
    var canvas = $canvas.get(0);
    var canvasData = canvas.toDataURL();
    if (canvasData === 'data:,' || canvasData.length < 10) {
        log('unsupported canvas.toDataURL method detected. trying using plugin');
        if (!window.canvaspluginTEMP) {
            showErrorMessage('plugin not connect :(');
        }
        var canvasHolder = $('#paint-holder').get(0);
        log('offset: t l w h:' + canvasHolder.offsetTop + '  ' + canvasHolder.offsetLeft + '  ' + canvasHolder.offsetWidth + '  ' + canvasHolder.offsetHeight + ' window.innerWidth: ' + window.innerWidth);
        var offset = {
            left: $canvas.offset().left + 5,
            top: $canvas.offset().top + 5,
            width: $canvas.width() - 10,
            height: $canvas.height() - 10
        };
        setTimeout(function () {
            window.canvaspluginTEMP(canvas, offset, 'png', function (val) {
                canvasData = val.data;
                log('canvas plugin image created');
                process(canvasData);
            });
        }, 100);

    } else {
        process(canvasData);
    }
    function process(canvasData) {
        var imageData = canvasData.replace(/data:image\/png;base64,/, '');
        var $adultForm = $('#adult-form');
        var user_id = sww.user.id;
        var full_name = $adultForm.find('input[name=full_name]').val();
        var date = $adultForm.find('input[name=date]').val();
        var printed_name = $adultForm.find('input[name=printed_name]').val();
        var formName = 'adult_form';
        var slipObject = {formName: formName, user_id: user_id, full_name: full_name, date: date, printed_name: printed_name, img: imageData};
        sww.slipObjectsArray.push(slipObject);
        sww.DataCount++;
        addDataToStore();
        writeStatistic();
        
    }
    return false;
}


//=============================  Save Minor  =============================
function saveMinorData() {
    var data = tp.getData();
    if (!(data && isArray(data) && data.length > 0)) {
        showErrorMessage(eMsg.enterSignature);
        return false;
    }
//==================full_name==================\\
    var $full_name = $('#full_name');
    if (hasFieldError($full_name)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($full_name);
    }
    if ($full_name.val() === '') {

    }
    //==================full_child_name==================\\
    var $full_child_name = $('#full_child_name');
    if (hasFieldError($full_child_name)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($full_child_name);
    }
    if ($full_child_name.val() === '') {

    }
    //==================born_year==================\\
    var $born_year = $('#born_year');
    if (hasFieldError($born_year)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($born_year);
    }
    if ($born_year.val() === '') {

    }
//==================date==================\\
    var $date = $('#date');
    if (hasFieldError($date)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($date);
    }
    if ($date.val() === '') {

    }

    //==================printed name==================\\
    var $printed_name = $('#printed_name');
    if (hasFieldError($printed_name)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($printed_name);
    }
    if ($printed_name.val() === '') {

    }
    //==================contact_info==================\\
    var $contact_info = $('#contact_info');
    if (hasFieldError($contact_info)) {
        showErrorMessage(eMsg.fieldBlankError);
        return false;
    } else {
        removeErrorFromField($contact_info);
    }
    if ($contact_info.val() === '') {

    }
    var $canvas = $('#touchpaint');
    var canvas = $canvas.get(0);
    var canvasData = canvas.toDataURL();
    if (canvasData === 'data:,' || canvasData.length < 10) {
        log('unsupported canvas.toDataURL method detected. trying using plugin');
        if (!window.canvaspluginTEMP) {
            showErrorMessage('plugin not connect :(');
        }
        var canvasHolder = $('#paint-holder').get(0);
        log('offset: t l w h:' + canvasHolder.offsetTop + '  ' + canvasHolder.offsetLeft + '  ' + canvasHolder.offsetWidth + '  ' + canvasHolder.offsetHeight + ' window.innerWidth: ' + window.innerWidth);
        var offset = {
            left: $canvas.offset().left + 5,
            top: $canvas.offset().top + 5,
            width: $canvas.width() - 10,
            height: $canvas.height() - 10
        };
        setTimeout(function () {
            window.canvaspluginTEMP(canvas, offset, 'png', function (val) {
                canvasData = val.data;
                log('canvas plugin image created');
                process(canvasData);
            });
        }, 100);

    } else {
        process(canvasData);
    }

    //log('after async');
    function process(canvasData) {
        var imageData = canvasData.replace(/data:image\/png;base64,/, '');
        var $minorForm = $('#minor-form');
        var user_id = sww.user.id;
        var full_name = $minorForm.find('input[name=full_name]').val();
        var full_child_name = $minorForm.find('input[name=full_child_name]').val();
        var born_year = $minorForm.find('input[name=born_year]').val();
        var date = $minorForm.find('input[name=date]').val();
        var printed_name = $minorForm.find('input[name=printed_name]').val();
        var contact_info = $minorForm.find('input[name=contact_info]').val();
        var slipObject = {user_id: user_id, full_name: full_name, full_child_name: full_child_name, born_year: born_year, date: date, printed_name: printed_name, contact_info: contact_info, img: imageData};
        sww.slipObjectsArray.push(slipObject);
        sww.DataCount++;
        addDataToStore();
        writeStatistic();
        loadContent('main');
    }
    return false;
}
//============================   Submits   ===========================
function submitAdultForm() {
    var slipNumbersCount = sww.slipObjectsArray.length;
    if (slipNumbersCount < 1) {
        showMessage(aMsg.nothingToSend, 'information');
        return;
    }
    var sendedSignaturesCount = 0;
    var formName = sww.slipObjectsArray[0].formName;
    if ((slipNumbersCount > 0) && (formName === 'adult_form')) {
        $.blockUI(blockParams);
        sendOneSignature();
    }
    else
    {
        SendData();
    }

    function sendOneSignature() {
        $('#block-text').html(sendedSignaturesCount + ' / ' + slipNumbersCount);
        if (sww.slipObjectsArray.length < 1) {
            $.unblockUI();
            showMessage(aMsg.signaturesSended, 'success');
            return false;
        }
        var id = sww.slipObjectsArray[0].user_id;
        var imageData = sww.slipObjectsArray[0].img;
        var full_name = sww.slipObjectsArray[0].full_name;
        var date = sww.slipObjectsArray[0].date;
        var printed_name = sww.slipObjectsArray[0].printed_name;
        var formName = sww.slipObjectsArray[0].formName;
        var data = {};
        data.user_id = id;
        data.full_name = full_name;
        data.image = imageData;
        data.date = date;
        data.printed_name = printed_name;
        data.formName = formName;
        uploadImageWithoutFile(data, afterSendingSignature);
    }

    function afterSendingSignature(result) {
        if (result.status.error) {
            showErrorMessage(result.error);
            $.unblockUI();
            return;
        }
        sendedSignaturesCount++;
        $('#block-text').html(sendedSignaturesCount + ' / ' + slipNumbersCount);
        sww.slipObjectsArray.shift();  // deleting first slip
        addDataToStore();
        sww.DataCount--;
        writeStatistic();
        sendOneSignature();
        loadContent('main');
        
    }

}

function submitMinorForm() {
    var slipNumbersCount = sww.slipObjectsArray.length;
    if (slipNumbersCount < 1) {
        showMessage(aMsg.nothingToSend, 'information');
        return;
    }

    var sendedSignaturesCount = 0;

    if (slipNumbersCount > 0) {
        $.blockUI(blockParams);
        sendOneSignature();
    }

    function sendOneSignature() {
        $('#block-text').html(sendedSignaturesCount + ' / ' + slipNumbersCount);
        if (sww.slipObjectsArray.length < 1) {
            $.unblockUI();
            showMessage(aMsg.signaturesSended, 'success');
            return false;
        }
        var id = sww.slipObjectsArray[0].user_id;
        var imageData = sww.slipObjectsArray[0].img;
        var full_name = sww.slipObjectsArray[0].full_name;
        var full_child_name = sww.slipObjectsArray[0].full_child_name;
        var born_year = sww.slipObjectsArray[0].born_year;
        var date = sww.slipObjectsArray[0].date;
        var printed_name = sww.slipObjectsArray[0].printed_name;
        var contact_info = sww.slipObjectsArray[0].contact_info;
        var data = {};
        data.user_id = id;
        data.full_name = full_name;
        data.full_child_name = full_child_name;
        data.born_year = born_year;
        data.image = imageData;
        data.date = date;
        data.printed_name = printed_name;
        data.contact_info = contact_info;
        uploadImageWithoutFile(data, afterSendingSignature);
    }

    function afterSendingSignature(result) {
        if (result.status.error) {
            showErrorMessage(result.error);
            $.unblockUI();
            return;
        }
        sendedSignaturesCount++;
        $('#block-text').html(sendedSignaturesCount + ' / ' + slipNumbersCount);
        sww.slipObjectsArray.shift();  // deleting first slip
        addDataToStore();
        sww.DataCount--;
        writeStatistic();
        sendOneSignature();
        loadContent('main');
    }
}

//============================   upload signatures   ===========================
function sendAdultForm() {
    saveAdultData();
    submitAdultForm();
}
function sendMinorForm() {
    saveMinorData();
    submitMinorForm();
}

//=============================   check login form   ===========================

function submitLoginForm() {
    var checked = true;
    var $loginForm = $('#login-form');
    var $userLogin = $loginForm.find('input[name=login]');
    var $userPwd = $loginForm.find('input[name=password]');

    if (hasFieldError($userLogin)) {
        checked = false;
    }

    if (hasFieldError($userPwd)) {
        checked = false;
    }

    if (checked) {
        authorizeOnServer(afterGettingResponse);
        function afterGettingResponse(result) {
            if (result.status.error) {
                if (result.error.login) {
                    setErrorToField($userLogin, result.error.login);
                    return;
                }
                if (result.error.password) {
                    setErrorToField($userPwd, result.error.password);
                    return;
                }

                if (result.error) {
                    showErrorMessage(result.error);
                    return;
                }
            }

            sww.user = result.data;
            sww.user.isLogged = true;
            writeLoginData();
            loadContent('main');

        }
    }

}
//================================  loadContent  ======================================
function loadContent(page) {
    if (page === 'main') {
        $('#body').load('main.html #inner-body', function () {
            Complete_form();
        });

    }
    if (page === 'index') {  //  index is login page
        $('#body').load('index.html #inner-body', function () {
            sww.user.isLogged = false;
            writeLoginData();
        });
    }
    if (page === 'adult_form') {
        $('#body').load('adult_form.html #inner-body', function () {
            showSignatureTab();
        });

    }
    if (page === 'minor_form') {
        $('#body').load('minor_form.html #inner-body', function () {
            showSignatureTab();
        });
    }
}

function Complete_form() {
    var count = sww.slipObjectsArray.length;
    if (count > 0)
    {
        $('#complete_form').load('main.html #sinhronize', function () {
        });
    }
}
function SendData() {
    var formName = sww.slipObjectsArray[0].formName;
    if (formName === "adult_form")
    {
        submitAdultForm();
    }
    else
    {
        submitMinorForm();
    }
}









$(document).ready(function () {

    log('document ready');
    initApp();
});


function initApp() {
    fixHeight();
    readSavedLoginData();
    if (sww.user && sww.user.isLogged === true) {
        loadContent('main');
    } else {

    }

    readSlipObjects();
    readHost();

}




//function initAdultForm(){
//    var currDate=new Date();
//    $('input[name=day]').val(currDate.getDate());
//    $('input[name=month]').val(currDate.getMonth()+1);
//    $('input[name=year]').val(currDate.getYear()-100);
//}
//function initMinorForm(){
//      var currDate=new Date();
//    $('input[name=day]').val(currDate.getDate());
//    $('input[name=month]').val(currDate.getMonth()+1);
//    $('input[name=year]').val(currDate.getYear()-100);
//}


function fixHeight() {
    var currHeight = $('body').height();
    $('body').css('height', currHeight);
}

function fixHover() {
    $('.btn').on('touchstart', function () {
        if ($(this).hasClass('btn')) {
            $(this).addClass('btn-hover');
        }
    });
    $('.btn').on('touchend', function () {
        if ($(this).hasClass('btn-hover')) {
            $(this).removeClass('btn-hover');
        }
    });
}

//===========================  cordova js init  ================================
var deviceIsReady = false;
function onDeviceReady() {
    log('Device is ready');
    deviceIsReady = true;
    navigator.splashscreen.hide();
    document.addEventListener('backbutton', function (e) {
        e.preventDefault();
        $('section:visible').find('.back-button').click();
    }, false);

    document.addEventListener('menubutton', function (e) {
        e.preventDefault();

        function onConfirm(buttonIndex) {
            if (buttonIndex === 2) {
                exitFromApp();
            }
        }

        navigator.notification.confirm(
                'Exit from app?',
                onConfirm,
                'Exit',
                'Cancel, Ok'
                );
    }, false);

}
document.addEventListener("deviceready", onDeviceReady, false);
function isDeviceReady() {
    if (deviceIsReady === false) {
        showErrorMessage('device not ready');
        log('device not ready');
        return false;
    }
    return true;
}
function exitFromApp()
{
    if (navigator.app && navigator.app.exitApp) {
        navigator.app.exitApp();
    } else if (navigator.device && navigator.device.exitApp) {
        navigator.device.exitApp();
    }
}
function addSlipNumberToView(slipNumber) {
//    if (ptrn.slipPtrn === null) {
//        ptrn.slipPtrn = getPattern($('#slip-pattern'));
//        $('#slip-pattern').remove();
//    }
//    var pattern = ptrn.slipPtrn;
//    var html = pattern.prop('outerHTML').render({slipNumber: slipNumber});
//    $('#slip-numbers-list').html(html);

    $('input.num').val(slipNumber);
}

function hasFieldError(inputField) {
    var val = inputField.val();
    if (val.length === 0) {
        setErrorToField(inputField, eMsg.fieldBlankError);
        return true;
    }

    if (val.length < 1) {
        setErrorToField(inputField, eMsg.fieldTooShortError);
        return true;
    }

    if (val.length > 25) {
        setErrorToField(inputField, eMsg.fieldTooLongError);
        return true;
    }

    removeErrorFromField(inputField);
    return false;
}

function setErrorToField($inputField, msg) {
    var $container = $inputField.parent('.row');
    var $errorTxtCntr = $container.find('.errMsg');
    $container.addClass('error');
    $errorTxtCntr.html(msg);
}

function removeErrorFromField($inputField) {
    var $container = $inputField.parent('.row');
    var $errorTxtCntr = $container.find('.errMsg');
    $container.removeClass('error');
    $errorTxtCntr.html('');
}



//==========================  buttons handlers  ================================

function gotoSignature() {
    var currentValue = getCurrentSlipNumber();
    // check for empty field
    if (currentValue.length === 0) {
        showErrorMessage(eMsg.scanBarcodeFirst);
        return false;
    }

    // check to pattern
    var re = /^[0-9]{5}_[0-9]{1,9}$/;
    var isCorrect = re.test(currentValue);
    if (!isCorrect) {
        showErrorMessage(eMsg.codeIncorrect);
        return false;
    }

    //check to companyId
//    var scannedCompanyId = currentValue.substr(0, currentValue.indexOf('_'));
//    if (sww.user.companyId !== scannedCompanyId) {
//        showErrorMessage(eMsg.companyNotCorrect + '"' + sww.user.companyId + '"');
//        return false;
//    }

    showSignatureTab();
    $('#signature-name-input').val("");
    $('#signature-slip-input').val(currentValue);
    return false;
}


function clearCanvas() {
    //sww.signatureImageData = null;
    tp.reset();
    return false;
}





function showLoginTab() {
    $('#login-tab').show().siblings().hide();
    return false;
}



function showSignatureTab() {
    $('#signature-tab').show().siblings().hide();
    fixHover();
    var $paintHolder = $('#paint-holder');
    if ($('#touchpaint').get(0)) {
        if (window.tp) {
            tp.clear();
        }
        return false;
    }
    var widthInPx = $paintHolder.width();
    var heightInPx = $paintHolder.height();
    var offsetTop = 0;
    var maxHeightInPx = widthInPx * 0.5;
    if (maxHeightInPx < heightInPx) {
        offsetTop = (heightInPx - maxHeightInPx) / 2;
        heightInPx = maxHeightInPx;
    }

    $paintHolder.html('<canvas width="' + widthInPx + '" height="' + heightInPx + '" id="touchpaint" class="canvas" style="top:' + offsetTop + 'px;"></canvas>');

    // initailize canvas widget
    window.tp = new TouchPaint("touchpaint", {});
    var $canvas = $('#touchpaint');
    var cWidth = $canvas.width();
    var boldLineScaleFactor = 90;
    var lineSize = cWidth / boldLineScaleFactor;
    tp.setStrokeSize(lineSize);
    //tp.setStrokeSize(5);
    setTimeout(function () {
        tp.clear();
    }, 100);
    return false;
}

var drawNameObj = {
   
};

function initDrawName() {
    var $canvas = $('#touchpaint');
    var canvas = $canvas.get(0);
    var context = canvas.getContext("2d");
    var cWidth = $canvas.width();
    var cHeight = $canvas.height();

    drawNameObj.cWidth = cWidth;
    drawNameObj.cHeight = cHeight;
    drawNameObj.context = context;

    var fontScaleFactor = 188;

    var fontSize = cWidth / fontScaleFactor;


    context.fillStyle = "black";
    context.font = fontSize + "em Arial";
    context.textAlign = "right";
    context.textBaseline = "bottom";


    drawName();
}
function drawName() {
    drawNameObj.context.fillText(drawNameObj.cWidth - 5, drawNameObj.cHeight - 5);
}

function showSendTab() {
    $('#send-tab').show().siblings().hide();
    fixHover();
    return false;
}

function logout() {
    if (sww.slipObjectsArray.length > 0) {
        if (!confirm(aMsg.logoutConfirm)) {
            return false;
        }
    }
    loadContent('index');
    deleteSlipData();
    return false;
}












//==========================  login data  ======================================

function readSavedLoginData() {
    var userJsonStr = store.getItem("user");
    if (userJsonStr) {
        var user = new User(JSON.parse(userJsonStr));
        sww.user = user;
        if (sww.user) {
            $('#login-form input[name=login]').val(sww.user.login);
            //$('#login-form input[name=password]').val(sww.user.password);
        }
    }
}


function writeLoginData() {
    store.setItem("user", JSON.stringify(sww.user));
}

function removeloginData() {
    store.removeItem("user");
}


//=======================  store slip number objects  ==========================

function readSlipObjects() {
    var slipObjectsArrayJsonStr = store.getItem('slipObjectsArray');
    if (slipObjectsArrayJsonStr) {
        var slipObjectsArray = JSON.parse(slipObjectsArrayJsonStr);
        sww.slipObjectsArray = slipObjectsArray;
    }
}



function addDataToStore() {
    store.setItem('slipObjectsArray', JSON.stringify(sww.slipObjectsArray));
}



//=========================  statistic data  ===================================


function writeStatistic() {
    var statObject = sww.DataCount;
    store.setItem('statistic', JSON.stringify(statObject));
}


/* function updateStatisticView() {
 $('#data-count').html(sww.DataCount);
 }
 */




function deleteSlipData() {
    sww.slipObjectsArray = [];
    addDataToStore();
    sww.deliveriesCount = 0;
    sww.signaturesCount = 0;
    writeStatistic();
}



//==========================    Settings   =====================================

function readHost() {
    var storedHost = store.getItem('host');
    if (storedHost) {
        sww.host = storedHost;
    } else {
        sww.host = sww.originalHost;
    }
}

function saveSettings() {
    var $modifiedHost = $('#url-input');
    var modifiedHost = $modifiedHost.val();
    if (checkUrl(modifiedHost) === false) {
        setErrorToField($modifiedHost, eMsg.incorrectUrl);
        return false;
    } else {
        removeErrorFromField($modifiedHost);
    }
    sww.host = modifiedHost;
    store.setItem('host', modifiedHost);
    showMessage(aMsg.settingsSaved, 'information');
}


function resetSettings() {
    sww.host = sww.originalHost;
    store.removeItem('host');
    $('#url-input').val(sww.host);

    //  no need in last t3
//    sww.user.modifiedName = sww.user.name;
//    $('#name-input').val(sww.user.modifiedName);
//
//    sww.user.modifiedCompany = sww.user.company;
//    $('#company-name-input').val(sww.user.modifiedCompany);
//    writeLoginData();


    showMessage(aMsg.settingsResetted, 'information');
}


function alternateLogin() {
    submitLoginForm();
}


function checkUrl(urlStr) {
    var urlRegexp = /^http[s]?:\/\/\w+(\.\w+)*(:[0-9]+)?\/?(\/[.\w]*)*$/;
    var result = urlRegexp.test(urlStr);
    return result;
}


function showUrlDialog() {
    var modifiedHost = prompt(aMsg.enterUrl, sww.host);
    if (modifiedHost === null) {
        return false;
    }

    if (checkUrl(modifiedHost) === false) {
        showErrorMessage(eMsg.incorrectUrl);
        return false;
    }

    sww.host = modifiedHost;
    store.setItem('host', modifiedHost);
    showMessage(aMsg.settingsSaved, 'information');
}

function fakeSubmit() {
    hideKeyboard();
    return false;
}
   